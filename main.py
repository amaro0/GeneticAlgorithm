import numpy as np
import os
import csv
from random import randint
from Had import Had
from Greedy import Greedy
from Logger  import Logger
from GeneticAlgorithm import GeneticAlgorithm

POPULATION_SIZE = 100
NUMBER_OF_GENERATIONS = 100
MUTATION_PROPABILITY = 0.01
CROSSOVER_PROPABILITY = 0.7
TOURNAMENT_SIZE = 5


def prepareData(file):
    csvfile = open(file, 'r')
    csvFileArray = []
    for row in csv.reader(csvfile, delimiter='.'):
        csvFileArray.append(row)
    n = list(map(int,csvFileArray[0]))[0]

    array = np.loadtxt(open(file, "rb"), delimiter=".", skiprows=1)
    # print('array', array)
    distMatrix = array[:n,:]
    flowMatrix = array[n:,:]
    # print('n',n)
    # print('dist', distMatrix)
    # print('flow', flowMatrix)
    path, file = os.path.split(file)
    return Had(n, distMatrix, flowMatrix, file)

def generatePopulation(had):
    population = np.empty([POPULATION_SIZE,had.n])
    for j in range(0,POPULATION_SIZE):
        chromosome = np.zeros([1,had.n])
        for i in range(0, had.n):
            while True:
                random = randint(1, had.n)
                if random not in chromosome:
                    chromosome[0,i] = random
                    break
        population[j,:] = chromosome
    # np.set_printoptions(threshold=np.nan)
    # print(population)
    return population

# zrobic greedy
# zrobic mutecje na poziomie genu, obliczamy prawdopodobiesntwo dla kazdego allela i potem swap ok 1%
# for i in range(0,11):
#     had12 = prepareData("data/had12.csv")
#     populationHad12 = generatePopulation(had12)
#     ge_had12 = GeneticAlgorithm(populationHad12,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
#                                 MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'tournament',had12)
#     greedy_had20 = Greedy(had12)
#     result = greedy_had20.evaluate(randint(1,had12.n))
#     print(ge_had12.evaluate(),';',ge_had12.quality(result),';',ge_had12.quality(populationHad12[0,:]))
# print(ge_had12.evaluate())
# had12 = prepareData("data/had12.csv")
# populationHad12 = generatePopulation(had12)
# ge_had12 = GeneticAlgorithm(populationHad12,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
#                             MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'roulette',had12)
# greedy_had20 = Greedy(had12)
# result = greedy_had20.evaluate(randint(1,had12.n))
#
# print(ge_had12.evaluate())
# for i in range(0,11):
#     had14 = prepareData("data/had14.csv")
#     populationHad14 = generatePopulation(had14)
#     ge_had14 = GeneticAlgorithm(populationHad14,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
#                                 MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'tournament',had14)
#     greedy_had20 = Greedy(had14)
#     result = greedy_had20.evaluate(randint(1,had14.n))
#     print(ge_had14.evaluate(),';',ge_had14.quality(result),';',ge_had14.quality(populationHad14[0,:]))
# print(ge_had14.evaluate())

# for i in range(0,11):
#     had16 = prepareData("data/had16.csv")
#     populationHad16 = generatePopulation(had16)
#     ge_had16 = GeneticAlgorithm(populationHad16,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
#                                 MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'tournament',had16)
#     greedy_had20 = Greedy(had16)
#     result = greedy_had20.evaluate(randint(1,had16.n))
#     print(ge_had16.evaluate(),';',ge_had16.quality(result),';',ge_had16.quality(populationHad16[0,:]))

# for i in range(0,11):
#     had18 = prepareData("data/had18.csv")
#     populationHad18 = generatePopulation(had18)
#     ge_had18 = GeneticAlgorithm(populationHad18,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
#                                 MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'tournament',had18)
#     greedy_had20 = Greedy(had18)
#     result = greedy_had20.evaluate(randint(1,had18.n))
#     print(ge_had18.evaluate(),';',ge_had18.quality(result),';',ge_had18.quality(populationHad18[0,:]))
#
#
# for i in range(0,11):
#     had20 = prepareData("data/had20.csv")
#     populationHad20 = generatePopulation(had20)
#     ge_had20 = GeneticAlgorithm(populationHad20,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
#                                 MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'tournament',had20)
#     # print("had20 i ge:", ge_had20.evaluate())
#
#     greedy_had20 = Greedy(had20)
#     result = greedy_had20.evaluate(randint(1,had20.n))
#     # print("had20 i greedy:", ge_had20.quality(result))
#     # print("had20 i random:", ge_had20.quality(populationHad20[0,:]))
#     print(ge_had20.evaluate(),';',ge_had20.quality(result),';',ge_had20.quality(populationHad20[0,:]))








had20 = prepareData("data/had20.csv")
populationHad20 = generatePopulation(had20)
ge_had20 = GeneticAlgorithm(populationHad20,POPULATION_SIZE,NUMBER_OF_GENERATIONS,
                            MUTATION_PROPABILITY,TOURNAMENT_SIZE,CROSSOVER_PROPABILITY,'roulette',had20)
print(ge_had20.evaluate())




