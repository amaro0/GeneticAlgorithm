from random import randint
import random
import numpy as np
import math
from Logger import Logger

class GeneticAlgorithm:

    def __init__(self,population, populationSize, numberOfGenerations, mutationPropability, tournamentSize, crossoverPropability, selectionMethod, had):
        self.population = population
        self.populationSize = populationSize
        self.numberOfGenerations = numberOfGenerations
        self.mutationPropability = mutationPropability
        self.crossoverPropability = crossoverPropability
        self.tournamentSize = tournamentSize
        self.selectionMethod = selectionMethod
        self.had = had
        self.best = None
        self.worst = None

    def evaluate(self):
        logger = Logger(self.had.fileName)
        if self.selectionMethod == 'tournament':
            return self.evaluateTournament(logger)
        else:
            return self.evaluateRoulette(logger)

    def evaluateTournament(self,logger):
        for i in range(0, self.numberOfGenerations):
            newPopulation = []
            qualityGenerationSum = self.qualityGenerationSum(self.population)
            # print("GENERATION: ", i)
            for j in range(0, math.floor(self.populationSize / 2)):
                chromosomesToTournamentX = []
                chromosomesToTournamentY = []
                for k in range(0, self.tournamentSize):
                    chromosomesToTournamentX.append(randint(0, self.populationSize - 1))
                    chromosomesToTournamentY.append(randint(0, self.populationSize - 1))
                x = self.tournament(chromosomesToTournamentX)
                y = self.tournament(chromosomesToTournamentY)
                x, y = self.crossover(x, y)
                x = self.mutation(x)
                y = self.mutation(y)

                newPopulation.append(x)
                newPopulation.append(y)

            self.population = np.array(newPopulation)
            logger.writeRow(i, self.bestInGeneration(self.population),
                            self.average(qualityGenerationSum, self.population),
                            self.worstInGeneration(self.population))
            # print(self.average(self.population))
            # print(np.array(newPopulation).shape)
        return self.best

    def evaluateRoulette(self, logger):
        for i in range(0, self.numberOfGenerations):
            newPopulation = []
            qualityGenerationSum = self.qualityGenerationSum(self.population)
            qualities = self.qualities()
            probs = self.rouletteProbs(qualityGenerationSum,qualities)
            print("GENERATION: ", i)
            for j in range(0, math.floor(self.populationSize / 2)):

                x = self.roulette(probs)
                y = self.roulette(probs)

                x, y = self.crossover(x, y)
                x = self.mutation(x)
                y = self.mutation(y)

                newPopulation.append(x)
                newPopulation.append(y)
            self.population = np.array(newPopulation)
            logger.writeRow(i, self.bestInGeneration(self.population),
                            self.average(qualityGenerationSum, self.population),
                            self.worstInGeneration(self.population))
        return self.best


    def crossover(self,x, y):
        if random.random() < self.crossoverPropability:
            cutPlace = randint(1, self.had.n-1)
            # print('x : ',x)
            x1 = x[0:cutPlace]
            x2 = x[cutPlace:]
            y1 = y[0:cutPlace]
            y2 = y[cutPlace:]
            # print('x1 : ',x1)
            # print('x2 : ', x2)
            crossover1 = np.append(x1, y2)
            crossover2 = np.append(x2, y1)
            # print('cs1 before :',crossover1)
            crossover1 = self.crossoverRepair(crossover1)
            crossover2 = self.crossoverRepair(crossover2)
            # print('cs1 repaired : ', crossover1)
            # print('cs2 : ', crossover2)
            return crossover1, crossover2
        return x, y

    def crossoverRepair(self,chromosome):
        chromosomeUniqueAllels = np.unique(chromosome)
        if len(chromosome) > len(chromosomeUniqueAllels):
            returnArray = np.zeros([self.had.n])
            doubletArray = []
            for i in range(0, self.had.n):
                if chromosome[i] not in returnArray:
                    returnArray[i] = chromosome[i]
                    # print(returnArray)
                else:
                    doubletArray.append(chromosome[i])
            # print('returnArray: ',returnArray)
            for i in range(0, self.had.n):
                for j in range(0, self.had.n):
                    if j+1 not in returnArray:
                        if returnArray[i] == 0:
                            returnArray[i] = j+1
            # print(returnArray)
            return returnArray
        else:
            return chromosome

    def mutation(self,chromosome):
        for i in range(0, len(chromosome)):
            if random.random() < self.mutationPropability:
                randomAllel = randint(0, self.had.n - 1)
                tmp = chromosome[i]
                chromosome[i] = chromosome[randomAllel]
                chromosome[randomAllel] = tmp
        return chromosome

    def tournament(self, chromosomes):
        qualities = []
        # print(chromosomes)
        for i in range(0,len(chromosomes)):
            qualities.append(self.quality(self.population[chromosomes[i], :]))
        # print('qualities: ', qualities)
        # print('chromosomes: ', chromosomes)
        return self.population[chromosomes[np.argmin(qualities)],:]

    def quality(self, chromosome):
        result = 0
        for i in range(0, self.had.n):
            for j in range(0, self.had.n):
                result += self.had.distMatrix[i-1, j-1] * self.had.flowMatrix[math.floor(chromosome[i-1]) - 1 , math.floor(chromosome[j-1]) - 1]

        if self.best is None:
            self.best = result
        elif self.best > result:
            self.best = result

        if self.worst is None:
            self.worst = result
        elif self.worst < result:
            self.worst = result

        return result

    def average(self,sum,generation):
        return sum/len(generation)

    def bestInGeneration(self, generation):
        best = self.quality(generation[0])
        for i in range(1, len(generation)):
            tmp = self.quality(generation[i])
            if best > tmp:
                best = tmp
        return best

    def worstInGeneration(self, generation):
        worst = self.quality(generation[0])
        for i in range(1, len(generation)):
            tmp = self.quality(generation[i])
            if worst < tmp:
                worst = tmp
        return worst

    def qualityGenerationSum(self, generation):
        sum = 0
        for i in range(0, len(generation)):
            sum += self.quality(generation[i])
        return sum

    def qualities(self):
        qualities = []
        for i in range(0, len(self.population)):
            qualities.append(self.quality(self.population[i,:]))
        return qualities

    def roulette(self, probs):
            r = random.random()
            for (i, individual) in enumerate(self.population):
                if r <= probs[i]:
                    print(individual)
                    return individual


    def rouletteProbs(self,totalQuality,qualities):
        relQuality = [f / totalQuality for f in qualities]
        probs = [sum(relQuality[:i + 1]) for i in range(len(relQuality))]
        return probs
