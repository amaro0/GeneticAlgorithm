import csv
import os
class Logger:

    def __init__(self,fileName):
        self.fileName = fileName
        self.csv_file = open('results' + fileName, "w",newline='')
        self.fieldNames = ['generationNumber', 'bestScore', 'averageScore', 'worstScore']


    def writeRow(self, generationNumber, bestScore, averageScore, worstScore):
        # writer = csv.DictWriter(self.csv_file, delimiter=',',fieldnames=self.fieldNames)
        writer = csv.writer(self.csv_file,delimiter=',')
        # writer.write({'generationNumber': generationNumber,
        #                  'bestScore': bestScore,
        #                  'averageScore': averageScore,
        #                  'worstScore': worstScore})
        writer.writerow([generationNumber, bestScore, averageScore, worstScore])
