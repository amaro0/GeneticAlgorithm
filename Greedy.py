class Greedy:

    def __init__(self, had):
        self.had = had

    def evaluate(self,start):
        result = [start]
        for i in range(0,self.had.n-1):
            bestFitness = 0
            bestPosition = 0
            for j in range(0, self.had.n):
                if j + 1 not in result:
                    if bestFitness == 0:
                        bestFitness = self.had.distMatrix[i-1, j-1] * self.had.flowMatrix[i-1,j-1]
                        fitness = self.had.distMatrix[i - 1, j - 1] * self.had.flowMatrix[i - 1, j - 1]
                        bestPosition = j+1
                    else:
                        fitness=self.had.distMatrix[i-1, j-1] * self.had.flowMatrix[i-1,j-1]
                    if fitness < bestFitness:
                        bestFitness = fitness
                        bestPosition = j + 1
            result.append(bestPosition)
        return result
